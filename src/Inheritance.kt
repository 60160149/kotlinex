package org.kotlinlang.play

//Inheritance//
open class Dog {               //หากจะสืบทอดคลาสต้องใส่ open เพื่อเป็นการอนุญาติ
    open fun sayHello() {
        println("wow wow!")
    }
}
class Yorkshire : Dog() {
    override fun sayHello() {
        println("wif wif!")
    }
}
//fun main() {
//    val dog: Dog = Yorkshire()
//    dog.sayHello()
//}

open class Tiger(val origin: String) {
    fun sayHello() {
        println("A Tiger from $origin says: grrhh!")
    }
}
class SiberianTiger : Tiger("Siberian")

//fun main() {
//    val tiger: Tiger = SiberianTiger()
//    tiger.sayHello()
//}

open class Lion(val name: String, val origin: String) {
    fun sayHello() {
        println("$name, the lion from $origin says: grrhh!")
    }
}
class Asiatic(name: String) : Lion(name = name, origin = "India")

fun main() {
    val lion: Lion = Asiatic("Rufo")
    lion.sayHello()
}