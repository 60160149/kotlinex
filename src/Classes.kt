package org.kotlinlang.play

class Customer  // ประกาศ class แบบไม่กำหนดค่าอะไรเลย

class Contact(val id: Int, var email: String)  // ประกาศ class โดยมีพารามิเตอร์

fun main() {
    val customer = Customer()

    val contact = Contact(1, "mary@gmail.com")

    println(contact.id)    // เข้าถึง id
    contact.email = "oom@gmail.com"  // อับเดตค่า email
}
