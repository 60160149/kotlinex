package org.kotlinlang.play

//Default Parameter Values and Named Arguments //
fun printMessage(message: String): Unit { //รับพารามิเตอร์ที่เป็นString
    println(message)
}
fun printMessageWithPrefix(message: String, prefix: String = "Info") { //กำหนดพารามิเตอร์ Prefix มีค่าเริ่มต้นเป็น "Info"
    println("[$prefix] $message")
}
fun sum(x: Int, y: Int): Int { //คืนค่าเป็นจำนวนเต็ม
    return x + y
}
fun multiply ( a: Int, b: Int) = a * b

fun main2() {
    printMessage("Helloooooo!")
    printMessageWithPrefix("Hello", "Log")
    printMessageWithPrefix("Hello")
    printMessageWithPrefix(prefix = "Log", message = "Hello") //กำหนดชื่อและเปลี่ยนลำดับอาร์กิวเมนต์
    println(sum(1,2)) //แสดงค่าฟังก์ชัน sum
    println(multiply(2,4)) //แสดงค่าฟังก์ชัน multiply

}
// Infix Function //
fun main3() {
    infix fun Int.times(str: String) = str.repeat(this)
    println(2 times "Bye ")            // เรียกใช้ฟังก์ชัน infix

    val pair = "Ferrari" to "Katrina" //เรียกฟังก์ชัน infix จากไลบรารีที่มีอยู่แล้ว
    println(pair)

    infix fun String.onto(other: String) = Pair(this, other)
    val myPair = "McLaren" onto "Lucas"
    println(myPair)

    val sophia = Person("Sophia")
    val claudia = Person("Claudia")
    sophia likes claudia


}
class Person(val name: String) {
    val likedPeople = mutableListOf<Person>()
    infix fun likes(other: Person) { likedPeople.add(other) }
}

//Operator Functions//
fun main4() {
    operator fun Int.times(str: String) = str.repeat(this) // ใช้เหมือนฟังก์ชัน infix
    println(2 * "Bye ")                                       // สัญลักษณ์สำหรับ times() คือ *


    operator fun String.get(range: IntRange) = substring(range) //ฟังก์ชันที่ช่วยเข้าถึงString ได้ง่ายขึ้น
    val str = "Always forgive your enemies"
    println(str[0..18])
}

// Functions with vararg Parameters //
fun main() {
    fun printAll(vararg messages: String) { // ฟังก์ชันรับพารามิเตอร์ประเภท vararg
        for (m in messages) println(m)
    }
    printAll("Hello", "สวัสดี", "여보세요", "こんにちは") // ใส่พารามิเตอร์จำนวนเท่าใดก็ได้

    fun printAllWithPrefix(vararg messages: String, prefix: String) {
        for(m in messages) println(prefix + m)
    }
    printAllWithPrefix("Hello", "สวัสดี", "여보세요", "こんにちは",prefix = "Greeting: ")

    fun log(vararg entries: String) {
        printAll(*entries)
    }

}

