package org.kotlinlang.play

//----- Null Safety---- //
fun main() {
    var nerverNall: String = "This can't be null"        // ประกาศตัวแปรเป็น String ไม่สามารถกำหนดค่าเป็น null
//    nerverNall = null                                    // error
    var nullable: String? = "You can keep a null here"   // สามารถใส่ค่า null ได้
    nullable = null
    var inferredNonNull = "The compiler assumes non-null" // ไม่ประกาศชนิดตัวแปร ไม่สามารถใส่ค่า null ได้
//    inferredNonNull = null                                // error

    fun strLength(notNull: String): Int {                 // ประกาศฟังก์ชัน String ที่ไม่ใช่ค่า null
        return notNull.length
    }
    strLength(nerverNall)       // เรียกฟังก์ด้วย String สามารถใช้ได้
//    strLength(nullable)         // เรียกฟังก์ด้วย String? ไม่สามารถใช้ได้

//----Working with Nulls----/
    fun describeString(maybeString: String?): String {
        if(maybeString != null && maybeString.length > 0) {
            return "String of length ${maybeString.length}"
        } else {
            return "Empty or null string"
        }
    }

}