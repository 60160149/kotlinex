package org.kotlinlang.play

//for//
fun main() {
    val cakes = listOf("carrot", "cheese","chocolate")

    for(cake in cakes) {
        println("Yummy, it's a $cake cake!")
    }
}

//while and do-while//
fun eatACake() = println("Eat a Cake")
fun bakeACake() = println("Bake a Cake")

fun main1(args: Array<String>) {
    var cakesEaten = 0
    var cakesBaked = 0

    while (cakesEaten < 5) {
        eatACake()
        cakesEaten++
    }
    do {
        bakeACake()
        cakesBaked++
    } while (cakesBaked < cakesEaten)

}

//Iterators//
class Animal(val name: String)

class Zoo(val animals: List<Animal>) {
    operator fun iterator(): Iterator<Animal> {
        return animals.iterator()
    }
}
fun main3() {
    val zoo = Zoo(listOf(Animal("zebra"), Animal("lion")))

    for (animal in zoo) {
        println("Watch out, it's a ${animal.name}")
    }
}
